import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:remote_controlled_car/features/car_control/presentation/pages/car_control_view.dart';
import 'package:remote_controlled_car/features/nfc_tag/presentation/pages/nfc_config_view.dart';
import 'package:remote_controlled_car/features/nfc_tag/presentation/view_models/nfc_tag_view_model.dart';

import 'features/camera_stream/camera_stream_data_service_locator.dart';

class MainPage extends StatefulWidget {
  final CarControlView carControlView;

  MainPage({@required this.carControlView}) : assert(carControlView != null);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int pageState;

  @override
  void initState() {
    pageState = 0;
    super.initState();
  }

  void changePageState(int page) => setState(() => pageState = page);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Remote Controlled Car App',
          textAlign: TextAlign.center,
        ),
      ),
      body: Center(
        child: Column(
          children: [
            pageState == 0
                ? widget.carControlView
                : NfcConfigView(nfcTagViewModel: locator<NfcTagViewModel>()),
            GNav(
              color: Colors.grey,
              activeColor: Colors.black,
              gap: 5,
              padding: EdgeInsets.all(10),
              tabActiveBorder: Border.all(),
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              tabs: [
                const GButton(
                  icon: Icons.home_outlined,
                  text: 'Home',
                ),
                const GButton(
                  icon: Icons.nfc_outlined,
                  text: 'NFC',
                ),
              ],
              onTabChange: (int page) => changePageState(page),
            ),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
