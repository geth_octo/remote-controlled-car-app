import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:remote_controlled_car/core/connection/domain/connection.dart';
import 'package:universal_io/io.dart';
import 'package:wifi/wifi.dart';

class UdpConnectionImpl extends Connection {
  final StreamController<Uint8List> _udpStream = StreamController<Uint8List>();
  final StreamController<Uint8List> _udpSink = StreamController<Uint8List>();

  final int listeningPort;
  final int writingPort;
  final InternetAddress ipAddr;

  UdpConnectionImpl(
      {@required this.listeningPort,
      @required this.writingPort,
      @required this.ipAddr})
      : assert(listeningPort != null),
        assert(writingPort != null),
        assert(ipAddr != null) {
    sendInitReq().then((_) {
      RawDatagramSocket.bind(InternetAddress.anyIPv4, listeningPort)
          .then((RawDatagramSocket udpSocket) {
        udpSocket.forEach((RawSocketEvent event) {
          if (event == RawSocketEvent.read) {
            Datagram dg = udpSocket.receive();
            //print('size: ' + dg.data.length.toString());
            _udpStream.sink.add(dg.data);
          }
        });
        _udpSink.stream.listen(
            (Uint8List data) => udpSocket.send(data, ipAddr, writingPort));
      });
    });
  }

  Stream<Uint8List> get stream => _udpStream.stream;

  Sink<Uint8List> get sink => _udpSink.sink;

  Future<void> sendInitReq() async {
    http.Response res;
    String localIp = await Wifi.ip;

    while (res == null || res.statusCode != 200) {
      try {
        res = await http.get(Uri.parse(
            'http://${ipAddr.address}/start?ip=$localIp&port=$listeningPort'));
        print(res?.statusCode);
      } catch (e) {
        print(e);
        res = null;
      }
      await Future.delayed(Duration(seconds: 3));
    }
  }

  void dispose() {
    _udpStream.close();
    _udpSink.close();
  }
}
