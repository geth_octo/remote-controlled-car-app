import 'dart:async';

import 'package:flutter/material.dart';
import 'package:remote_controlled_car/core/connection/domain/connection.dart';
import 'package:web_socket_channel/io.dart';

class WebSocketConnectionImpl implements Connection {
  final String url;
  final StreamController<dynamic> _wsStream =
      StreamController<dynamic>.broadcast();
  IOWebSocketChannel _channel;

  WebSocketConnectionImpl({@required this.url}) : assert(url != null) {
    _connectWs();
  }

  @override
  Stream<dynamic> get stream => _wsStream.stream;

  @override
  Sink<dynamic> get sink => _wsStream.sink;

  Future<void> _connectWs() async {
    print("WS connection : $url");
    _channel = IOWebSocketChannel.connect(url);
    _channel.stream.listen((event) {
      _wsStream.add(event);
    }, onDone: () async {
      print('done : $url');
      await Future.delayed(const Duration(seconds: 5));
      _connectWs();
    }, onError: (error) => print('Connection failed - ' + error.toString()));
    _wsStream.stream.listen((data) => _channel.sink.add(data));
  }

  void dispose() => _wsStream.close();
}
