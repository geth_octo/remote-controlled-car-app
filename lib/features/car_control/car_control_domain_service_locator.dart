import 'package:get_it/get_it.dart';
import 'package:remote_controlled_car/features/car_control/domain/usecases/send_car_direction_usecase.dart';

GetIt locator = GetIt.instance;

void carControlDomainSetup() {
  // usecases
  locator.registerLazySingleton(
      () => SendCarDirectionUsecase(repository: locator()));
}
