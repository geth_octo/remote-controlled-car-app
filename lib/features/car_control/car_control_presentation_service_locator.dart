import 'package:get_it/get_it.dart';
import 'package:remote_controlled_car/core/check_connectivity.dart';
import 'package:remote_controlled_car/features/camera_stream/presentation/view_models/camera_stream_view_model.dart';
import 'package:remote_controlled_car/features/car_control/domain/usecases/send_car_direction_usecase.dart';
import 'package:remote_controlled_car/features/car_control/presentation/pages/car_control_view.dart';
import 'package:remote_controlled_car/features/car_control/presentation/view_models/car_control_view_model.dart';
import 'package:remote_controlled_car/features/car_control/presentation/view_models/voice_control_view_model.dart';
import 'package:remote_controlled_car/features/flash_led/presentation/view_models/flash_led_view_model.dart';
import 'package:speech_to_text/speech_to_text.dart';

GetIt locator = GetIt.instance;

void carControlPresentationSetup() {
  // views
  locator.registerLazySingleton(() => CarControlView(
      cameraStreamViewModel: locator<CameraStreamViewModel>(),
      carControlViewModel: locator<CarControlViewModel>(),
      voiceControlViewModel: locator<VoiceControlViewModel>(),
      flashLedViewModel: locator<FlashLedViewModel>()));

  // viewModels
  locator.registerLazySingleton(() => CarControlViewModel(
      sendCarDirection: locator<SendCarDirectionUsecase>()));

  locator.registerLazySingleton(() => VoiceControlViewModel(
      speech: SpeechToText(),
      sendCarDirectionUsecase: locator<SendCarDirectionUsecase>(),
      checkConnectivity: locator<CheckConnectivity>()));
}
