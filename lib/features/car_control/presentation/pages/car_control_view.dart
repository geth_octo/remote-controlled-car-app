import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:remote_controlled_car/features/camera_stream/presentation/view_models/camera_stream_view_model.dart';
import 'package:remote_controlled_car/features/camera_stream/presentation/widgets/camera_frame_widget.dart';
import 'package:remote_controlled_car/features/car_control/presentation/view_models/car_control_view_model.dart';
import 'package:remote_controlled_car/features/car_control/presentation/view_models/voice_control_view_model.dart';
import 'package:remote_controlled_car/features/car_control/presentation/widgets/joystick_widget.dart';
import 'package:remote_controlled_car/features/car_control/presentation/widgets/voice_control_button_widget.dart';
import 'package:remote_controlled_car/features/flash_led/presentation/view_models/flash_led_view_model.dart';
import 'package:remote_controlled_car/features/flash_led/presentation/widgets/flash_led_button_widget.dart';
import 'package:remote_controlled_car/features/image_classifier/widgets/image_classifier_button_widget.dart';
import 'package:remote_controlled_car/features/nfc_tag/presentation/view_models/nfc_tag_view_model.dart';

import '../../../../service_locator.dart';

class CarControlView extends StatelessWidget {
  final CameraStreamViewModel cameraStreamViewModel;
  final FlashLedViewModel flashLedViewModel;
  final CarControlViewModel carControlViewModel;
  final VoiceControlViewModel voiceControlViewModel;

  CarControlView(
      {@required this.carControlViewModel,
      @required this.flashLedViewModel,
      @required this.cameraStreamViewModel,
      @required this.voiceControlViewModel})
      : assert(carControlViewModel != null),
        assert(flashLedViewModel != null),
        assert(cameraStreamViewModel != null),
        assert(voiceControlViewModel != null);

  @override
  Widget build(BuildContext context) {
    locator<NfcTagViewModel>().initRead();
    return Container(
      child: Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Container(
                alignment: Alignment.topCenter,
                height: MediaQuery.of(context).size.height / 3,
                child: CameraFrameWidget(
                    imagesStream: cameraStreamViewModel.cameraStream),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  VoiceControlButtonWidget(
                      voiceControlViewModel: voiceControlViewModel),
                  const SizedBox(width: 20),
                  FlashLedButtonWidget(viewModel: flashLedViewModel),
                  const SizedBox(width: 20),
                  ImageClassifierButtonWidget(
                    imageClassifierViewModel:
                        cameraStreamViewModel.imageClassifierViewModel,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(bottom: 10, top: 10),
                child:
                    JoystickWidget(dataOutput: carControlViewModel.controlData),
              ),
            )
          ],
        ),
      ),
    );
  }
}
