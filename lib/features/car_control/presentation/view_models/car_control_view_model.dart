import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:remote_controlled_car/features/car_control/domain/models/car_control.dart';
import 'package:remote_controlled_car/features/car_control/domain/usecases/send_car_direction_usecase.dart';

class CarControlViewModel {
  final StreamController<List<double>> _controlData =
      StreamController<List<double>>();
  final SendCarDirectionUsecase sendCarDirection;

  Sink<List<double>> get controlData => _controlData.sink;

  CarControlViewModel({@required this.sendCarDirection})
      : assert(sendCarDirection != null) {
    sendCarDirection(_controlData.stream.map((newData) =>
        CarControl(direction: newData[0] - 180, speed: newData[1])));
  }

  void dispose() => _controlData.close();
}
