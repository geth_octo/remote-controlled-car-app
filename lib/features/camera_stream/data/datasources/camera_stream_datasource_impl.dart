import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:remote_controlled_car/core/connection/domain/connection.dart';
import 'package:remote_controlled_car/features/camera_stream/data/repositories/camera_stream_datasource.dart';

class CameraStreamDatasourceImpl extends CameraStreamDatasource {
  final Connection connection;

  CameraStreamDatasourceImpl({@required this.connection})
      : assert(connection != null);

  @override
  Stream<Uint8List> getCameraStream() => connection.stream;
}
