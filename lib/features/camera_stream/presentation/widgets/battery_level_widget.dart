import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BatteryLevelWidget extends StatefulWidget {
  @override
  _BatteryLevelWidgetState createState() => _BatteryLevelWidgetState();
}

class _BatteryLevelWidgetState extends State<BatteryLevelWidget>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  CurvedAnimation _curve;
  Animation<double> _width;
  Animation<Color> _color;

  @override
  void initState() {
    _controller =
        AnimationController(duration: Duration(minutes: 3), vsync: this);
    _curve = CurvedAnimation(parent: _controller, curve: Curves.easeInOutQuart);
    _width = Tween<double>(begin: 20.0, end: 0).animate(_controller);
    _color = ColorTween(begin: Colors.green, end: Colors.red).animate(_curve);
    _controller.addListener(() => setState(() {}));
    _controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.center,
      children: [
        RotatedBox(
          quarterTurns: 1,
          child: Icon(
            Icons.battery_std,
            color: Colors.white,
            size: 30,
          ),
        ),
        Positioned(
          left: 4,
          child: Opacity(
            opacity: 0.8,
            child: Container(
              color: _color.value,
              height: 10,
              width: _width.value,
            ),
          ),
        )
      ],
    );
  }
}
