import 'package:flutter/material.dart';
import 'package:remote_controlled_car/features/camera_stream/presentation/widgets/no_image_widget.dart';

class CameraFrameWidget extends StatelessWidget {
  final Stream<Widget> imagesStream;

  CameraFrameWidget({@required this.imagesStream})
      : assert(imagesStream != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: StreamBuilder(
        initialData: NoImageWidget(),
        stream: imagesStream,
        builder: (_, AsyncSnapshot<dynamic> snapshot) =>
            Container(child: snapshot.data),
      ),
    );
  }
}
