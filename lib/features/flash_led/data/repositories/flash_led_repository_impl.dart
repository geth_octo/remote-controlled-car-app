import 'package:flutter/cupertino.dart';
import 'package:remote_controlled_car/features/flash_led/data/repositories/flash_led_datasource.dart';
import 'package:remote_controlled_car/features/flash_led/domain/repositories/flash_led_repository.dart';

class FlashLedRepositoryImpl implements FlashLedRepository {
  FlashLedDatasource datasource;

  FlashLedRepositoryImpl({@required this.datasource})
      : assert(datasource != null);

  @override
  void setCarLedState(bool isLedOn) {
    datasource.sendFlashLedStateUpdate(isLedOn);
  }
}
