import 'package:get_it/get_it.dart';
import 'package:remote_controlled_car/features/flash_led/presentation/view_models/flash_led_view_model.dart';

GetIt locator = GetIt.instance;

void flashLedPresentationSetup() {
  // viewModel
  locator.registerFactory(() => FlashLedViewModel(usecase: locator()));
}
