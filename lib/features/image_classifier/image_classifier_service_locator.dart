import 'package:get_it/get_it.dart';
import 'package:remote_controlled_car/features/image_classifier/classifier.dart';
import 'package:remote_controlled_car/features/image_classifier/image_classifier.dart';
import 'package:remote_controlled_car/features/image_classifier/view_models/image_classifier_view_model.dart';

GetIt locator = GetIt.instance;

void imageClassifierSetup() {
  locator.registerLazySingleton(() => Classifier());

  locator.registerLazySingleton(
      () => ImageClassifier(classifier: locator<Classifier>()));

  locator.registerLazySingleton(
      () => ImageClassifierViewModel(imageClassifier: locator()));
}
