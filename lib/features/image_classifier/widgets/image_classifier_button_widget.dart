import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remote_controlled_car/features/image_classifier/view_models/image_classifier_view_model.dart';

class ImageClassifierButtonWidget extends StatelessWidget {
  final ImageClassifierViewModel imageClassifierViewModel;

  ImageClassifierButtonWidget({@required this.imageClassifierViewModel})
      : assert(imageClassifierViewModel != null);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: imageClassifierViewModel,
      builder: (BuildContext context, _) => TextButton(
        onPressed: Provider.of<ImageClassifierViewModel>(context)
            .toggleRecognitionState,
        child: Icon(
          Provider.of<ImageClassifierViewModel>(context).icon,
          size: 30,
          color: Colors.deepPurple,
        ),
        style: TextButton.styleFrom(
            padding: EdgeInsets.all(13),
            shape: CircleBorder(side: BorderSide(color: Colors.deepPurple))),
      ),
    );
  }
}
