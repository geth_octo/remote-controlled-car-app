import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:remote_controlled_car/features/image_classifier/image_classifier.dart';

/// Represents the recognition output from the model
class RecognitionModel {
  /// Index of the result
  final int _id;

  /// Label of the result
  final String _label;

  /// Confidence [0.0, 1.0]
  final double _score;

  /// Image width to calculate the ratio
  final double _imageWidth;

  /// Location of bounding box rect
  ///
  /// The rectangle corresponds to the raw input image
  /// passed for inference
  final Rect _location;

  RecognitionModel(
      this._id, this._label, this._score, this._imageWidth, this._location);

  int get id => _id;

  String get label => _label;

  double get score => _score;

  Rect get location => _location;

  /// Returns bounding box rectangle corresponding to the
  /// displayed image on screen
  ///
  /// This is the actual location where rectangle is rendered on
  /// the screen
  Rect get renderLocation {
    // ratioX = screenWidth / imageInputWidth
    // ratioY = ratioX if image fits screenWidth with aspectRatio = constant
    double ratioX = ImageClassifier.screenWidth / _imageWidth;
    double ratioY = ratioX;

    double transLeft = max(0.1, location.left * ratioX);
    double transTop = max(0.1, location.top * ratioY);
    double transWidth = location.width * ratioX;
    double transHeight = location.height * ratioY;

    Rect transformedRect =
        Rect.fromLTWH(transLeft, transTop, transWidth, transHeight);
    return transformedRect;
  }
}
