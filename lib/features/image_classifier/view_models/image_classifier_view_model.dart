import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:remote_controlled_car/features/image_classifier/image_classifier.dart';

class ImageClassifierViewModel with ChangeNotifier {
  final ImageClassifier imageClassifier;
  bool _recognitionOn = false;

  ImageClassifierViewModel({@required this.imageClassifier})
      : assert(imageClassifier != null);

  void toggleRecognitionState() {
    _recognitionOn = !_recognitionOn;
    notifyListeners();
  }

  IconData get icon => _recognitionOn
      ? Icons.visibility_off_outlined
      : Icons.visibility_outlined;

  bool get state => _recognitionOn;

  Widget getPredictionsForBinaryImage(Uint8List image) =>
      imageClassifier.runOnBinaryImage(imageData: image);
}
