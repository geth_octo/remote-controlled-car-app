import 'package:connectivity/connectivity.dart';
import 'package:get_it/get_it.dart';
import 'package:remote_controlled_car/core/check_connectivity.dart';
import 'package:remote_controlled_car/core/connection/data/mqtt_connection.dart';
import 'package:remote_controlled_car/features/camera_stream/camera_stream_data_service_locator.dart';
import 'package:remote_controlled_car/features/camera_stream/camera_stream_domain_service_locator.dart';
import 'package:remote_controlled_car/features/camera_stream/camera_stream_presentation_service_locator.dart';
import 'package:remote_controlled_car/features/car_control/car_control_data_service_locator.dart';
import 'package:remote_controlled_car/features/car_control/car_control_domain_service_locator.dart';
import 'package:remote_controlled_car/features/car_control/car_control_presentation_service_locator.dart';
import 'package:remote_controlled_car/features/flash_led/flash_led_data_service_locator.dart';
import 'package:remote_controlled_car/features/flash_led/flash_led_domain_service_locator.dart';
import 'package:remote_controlled_car/features/flash_led/flash_led_presentation_service_locator.dart';
import 'package:remote_controlled_car/features/image_classifier/image_classifier_service_locator.dart';
import 'package:remote_controlled_car/features/nfc_tag/presentation/view_models/nfc_tag_view_model.dart';
import 'package:universal_io/io.dart';

import 'core/connection/domain/connection.dart';

/* ESP STATION MODE */
InternetAddress carIp = InternetAddress("192.168.1.89");
//InternetAddress carIp = InternetAddress("192.168.5.244");

/* ESP AP MODE */
//InternetAddress carIp = InternetAddress("192.168.4.1");

const int listeningPort = 1234;
const int writingPort = 9999;

const String BLUETOOTH_SERVER_NAME = "Remote Controlled Car";

GetIt locator = GetIt.I;

void setup() {
  // CarControl
  carControlPresentationSetup();
  carControlDomainSetup();
  carControlDataSetup();

  // CameraStream
  cameraStreamPresentationSetup();
  cameraStreamDomainSetup();
  cameraStreamDataSetup();

  // FlashLed
  flashLedPresentationSetup();
  flashLedDomainSetup();
  flashLedDataSetup();

  // Image Classifier
  imageClassifierSetup();

  // NFC
  locator.registerLazySingleton<NfcTagViewModel>(
      () => NfcTagViewModel(sendCarDirectionUsecase: locator()));

  // Core
  /* ESP UDP CONNECTION MODE */
  /*locator.registerLazySingleton<Connection>(() => UdpConnectionImpl(
      ipAddr: carIp, writingPort: writingPort, listeningPort: listeningPort));*/

  /* ESP BLUETOOTH CONNECTION MODE */
  /*locator.registerLazySingleton<Connection>(() => BluetoothConnection(
      flutterBlue: FlutterBlue.instance, serverName: BLUETOOTH_SERVER_NAME));*/

  /* ESP MQTT CONNECTION MODE */
  locator.registerLazySingleton<Connection>(
      () => MqttConnection(brokerAddress: "192.168.1.122"));

  locator.registerLazySingleton(
      () => CheckConnectivity(connectivity: Connectivity()));
}
