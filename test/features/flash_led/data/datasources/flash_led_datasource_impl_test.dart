import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:remote_controlled_car/core/connection/data/udp_connection_impl.dart';
import 'package:remote_controlled_car/features/flash_led/data/datasources/flash_led_datasource_impl.dart';

class MockUdpConnectionImpl extends Mock implements UdpConnectionImpl {}

void main() {
  FlashLedDatasourceImpl datasource;
  MockUdpConnectionImpl mockConnection;

  setUp(() {
    mockConnection = MockUdpConnectionImpl();
    datasource = FlashLedDatasourceImpl(connection: mockConnection);
  });

  test('should emit the right json message on the Connection sink', () async {
    // Given
    final StreamController<Uint8List> udpConnectionStream =
        StreamController<Uint8List>();
    final bool isLedOn = true;
    final Uint8List expectedResult =
        Uint8List.fromList(json.encode({'isLedOn': isLedOn}).codeUnits);
    when(mockConnection.sink).thenReturn(udpConnectionStream.sink);

    // When
    datasource.sendFlashLedStateUpdate(isLedOn);

    // Then
    await expectLater(udpConnectionStream.stream, emits(expectedResult));

    udpConnectionStream.close();
  });
}
