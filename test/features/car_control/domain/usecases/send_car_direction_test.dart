import 'dart:async';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:remote_controlled_car/features/car_control/domain/models/car_control.dart';
import 'package:remote_controlled_car/features/car_control/domain/repositories/car_control_repository.dart';
import 'package:remote_controlled_car/features/car_control/domain/usecases/send_car_direction_usecase.dart';

class MockCarControlRepository extends Mock implements CarControlRepository {}

void main() {
  SendCarDirectionUsecase usecase;
  MockCarControlRepository mockCarControlRepository;

  setUp(() {
    mockCarControlRepository = MockCarControlRepository();
    usecase = SendCarDirectionUsecase(repository: mockCarControlRepository);
  });

  test('should call the repository method with the right stream', () {
    // Given
    final Stream<CarControl> stream = StreamController<CarControl>().stream;

    // When
    usecase(stream);

    // Then
    verify(mockCarControlRepository.sendCarDirection(stream));
  });
}
